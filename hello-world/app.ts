import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import middy from '@middy/core';
import validateAuthorizationHeader from './src/presentation/middleware/validate-authorization-header.middleware';
import validateMerchantMiddleware from './src/presentation/middleware/validate-merchant.middleware';
import errorFilterMiddleware from './src/presentation/middleware/error-filter.middleware';
import { ZodError, z } from 'zod';
import validateAndParseBody from './src/presentation/middleware/validate-and-parse-body.middleware';
import ValidateChargeRequestDto, {
    validateChargeRequestSchema,
} from './src/application/dto/validate-charge-request.dto';
/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */

const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'its a work in progress still, stay tuned for updates!!',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: JSON.stringify(err),
            }),
        };
    }
};

export const lambdaHandler = middy(handler)
    .use(validateAuthorizationHeader())
    .use(validateMerchantMiddleware())
    .use(validateAndParseBody<ValidateChargeRequestDto>(validateChargeRequestSchema))
    .use(errorFilterMiddleware());
