import { inject, injectable } from 'inversify';
import UseCase from '../../shared/application/use-case';
import GetMerchantRequestDto from '../dto/get-merchant-request.dto';
import GetMerchantResponseDto from '../dto/get-merchant-response.dto';
import MerchantKeyRepository from '../../domain/repository/merchant-key.repository';
import TYPES from '../../types';
import { CulqiError } from '../../shared/culqi.error';
import { Response } from '../../shared/constants';

@injectable()
export default class GetMerchantUseCase implements UseCase<GetMerchantRequestDto, GetMerchantResponseDto> {
    constructor(@inject(TYPES.MockedMerchantKeyRepository) private _repository: MerchantKeyRepository) {}

    async execute(input: GetMerchantRequestDto): Promise<GetMerchantResponseDto> {
        const merchantKey = await this._repository.get(input.key);

        if (!merchantKey) {
            throw new CulqiError(Response.AUTHORIZATION_FOUND_BUT_NO_API_KEY);
        }

        if (!merchantKey.active) {
            throw new CulqiError(Response.SECURE_KEY_INACTIVE);
        }

        const result = new GetMerchantResponseDto();
        result.key = input.key;
        result.active = merchantKey.active;

        return result;
    }
}
