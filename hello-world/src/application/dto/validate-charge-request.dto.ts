import { z } from 'zod';
import { Response } from '../../shared/constants';

export const schema = 1;

const validCurrencies = ['PEN', 'USD'];

export const validateChargeRequestSchema = z.object({
    amount: z.coerce
        .number({
            required_error: `${Response.INVALID_AMOUNT}`,
            invalid_type_error: `${Response.INVALID_AMOUNT_LESS_THAN_9999900}`,
        })
        .min(100, `${Response.INVALID_AMOUNT_GREATHER_THAN_100}`)
        .max(9999900, `${Response.INVALID_AMOUNT_LESS_THAN_9999900}`)
        .transform((x) => x.toString()),
    applicationFee: z.coerce
        .number({
            invalid_type_error: `${Response.INVALID_APPLICATION_FEE}`,
        })
        .min(1, `${Response.INVALID_APPLICATION_FEE}`)
        .max(5000000, `${Response.INVALID_APPLICATION_FEE}`)
        .optional()
        .transform((x) => x?.toString()),
    currency: z
        .string({
            required_error: `${Response.INVALID_CURRENCY_CODE}`,
        })
        .min(1, `${Response.INVALID_CURRENCY_CODE}`)
        .refine(
            (currency) => validCurrencies.some((validCurrency) => validCurrency === currency),
            `${Response.INVALID_CURRENCY_CODE}`,
        ),
});

type ValidateChargeRequestDto = z.infer<typeof validateChargeRequestSchema>;

export default ValidateChargeRequestDto;
