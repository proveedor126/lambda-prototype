export default class GetMerchantResponseDto {
    active: boolean;
    key: string;
}
