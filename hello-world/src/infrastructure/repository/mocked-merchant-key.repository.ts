import { injectable } from 'inversify';
import MerchantKeyEntity from '../../domain/entity/merchant-key.entity';
import MerchantKeyRepository from '../../domain/repository/merchant-key.repository';

const mockedData: { [key: string]: { active: boolean } } = {
    '1234': {
        active: true,
    },
    '5678': {
        active: false,
    },
};

@injectable()
export default class MockedMerchantKeyRepository implements MerchantKeyRepository {
    async get(key: string): Promise<MerchantKeyEntity | undefined> {
        const result = mockedData[key];

        if (!result) {
            return undefined;
        }

        const merchantKey = new MerchantKeyEntity(key, result.active);

        return merchantKey;
    }
}
