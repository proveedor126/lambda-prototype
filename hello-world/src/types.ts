const TYPES = {
    MockedMerchantKeyRepository: Symbol('MockedMerchantKeyRepository'),
    GetMerchantUseCase: Symbol('GetMerchantUseCase'),
};

export default TYPES;
