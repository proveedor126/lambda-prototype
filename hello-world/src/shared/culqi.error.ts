export class CulqiError extends Error {
    constructor(public responseCode: number) {
        super();
    }
}
