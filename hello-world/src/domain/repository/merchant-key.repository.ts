import MerchantKeyEntity from '../entity/merchant-key.entity';

export default interface MerchantKeyRepository {
    get(key: string): Promise<MerchantKeyEntity | undefined>;
}
