export default class MerchantKeyEntity {
    key: string;
    active: boolean;

    constructor(key: string, active: boolean) {
        this.key = key;
        this.active = active;
    }
}
