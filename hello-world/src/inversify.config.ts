import 'reflect-metadata';
import { Container } from 'inversify';
import MerchantKeyRepository from './domain/repository/merchant-key.repository';
import TYPES from './types';
import MockedMerchantKeyRepository from './infrastructure/repository/mocked-merchant-key.repository';
import GetMerchantRequestDto from './application/dto/get-merchant-request.dto';
import GetMerchantResponseDto from './application/dto/get-merchant-response.dto';
import GetMerchantUseCase from './application/use-case/get-merchant.use-case';
import UseCase from './shared/application/use-case';

const container: Container = new Container();

container.bind<MerchantKeyRepository>(TYPES.MockedMerchantKeyRepository).to(MockedMerchantKeyRepository);
container.bind<UseCase<GetMerchantRequestDto, GetMerchantResponseDto>>(TYPES.GetMerchantUseCase).to(GetMerchantUseCase);

export default container;
