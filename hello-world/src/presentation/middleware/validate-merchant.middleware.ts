import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import container from '../../inversify.config';
import UseCase from '../../shared/application/use-case';
import GetMerchantRequestDto from '../../application/dto/get-merchant-request.dto';
import GetMerchantResponseDto from '../../application/dto/get-merchant-response.dto';
import TYPES from '../../types';

const validateMerchantMiddleware = (): middy.MiddlewareObj<APIGatewayProxyEvent, APIGatewayProxyResult> => {
    const before: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult> = async (request): Promise<void> => {
        const merchantKey = request.internal.merchantKey as string;

        const useCase = container.get<UseCase<GetMerchantRequestDto, GetMerchantResponseDto>>(TYPES.GetMerchantUseCase);

        const requestDto = new GetMerchantRequestDto();
        requestDto.key = merchantKey;

        const result = await useCase.execute(requestDto);

        request.internal.merchant = result;
    };

    return {
        before,
    };
};

export default validateMerchantMiddleware;
