import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { CulqiError } from '../../shared/culqi.error';
import { Response } from '../../shared/constants';

const errorFilterMiddleware = (): middy.MiddlewareObj<APIGatewayProxyEvent, APIGatewayProxyResult> => {
    const onError: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult, unknown> = async (
        request,
    ): Promise<void> => {
        try {
            const error = request?.error as CulqiError;

            if (!request.error) {
                request.response = {
                    statusCode: 500,
                    body: '{}',
                };
            } else {
                let statusCode = 500;

                switch (error.responseCode) {
                    case Response.NO_AUTHORIZATION_HEADER:
                    case Response.AUTHORIZATION_FOUND_BUT_NO_API_KEY:
                    case Response.SECURE_KEY_INACTIVE:
                        statusCode = 400;
                        break;
                }

                request.response = {
                    statusCode,
                    body: JSON.stringify({
                        responseCode: error.responseCode,
                    }),
                };
            }
        } catch {
            request.response = {
                statusCode: 500,
                body: '{}',
            };
        }
    };

    return {
        onError,
    };
};

export default errorFilterMiddleware;
