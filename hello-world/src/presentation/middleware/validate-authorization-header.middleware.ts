import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { CulqiError } from '../../shared/culqi.error';
import { Response } from '../../shared/constants';

const validateAuthorizationHeader = (): middy.MiddlewareObj<APIGatewayProxyEvent, APIGatewayProxyResult> => {
    const before: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult> = async (request): Promise<void> => {
        const authorization = request.event.headers['Authorization'];

        if (!authorization) {
            throw new CulqiError(Response.NO_AUTHORIZATION_HEADER);
        }

        if (!authorization.startsWith('Bearer ')) {
            throw new CulqiError(Response.AUTHORIZATION_FOUND_BUT_NO_API_KEY);
        }

        request.internal.merchantKey = authorization.substring(7);
    };

    return {
        before,
    };
};

export default validateAuthorizationHeader;
