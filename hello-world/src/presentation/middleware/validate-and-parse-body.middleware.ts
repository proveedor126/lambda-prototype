import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { ParseParams, ZodError } from 'zod';
import { CulqiError } from '../../shared/culqi.error';

interface ValidateSchema<T> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    parse(data: unknown, params?: Partial<ParseParams>): T;
}

const validateAndParseBodyMiddleware = <T>(
    schema: ValidateSchema<T>,
): middy.MiddlewareObj<APIGatewayProxyEvent, APIGatewayProxyResult> => {
    const before: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult> = async (request): Promise<void> => {
        if (request?.event?.body) {
            try {
                const bodyRequest = schema.parse(JSON.parse(request?.event?.body));
                request.internal.request = bodyRequest;
            } catch (err) {
                const validationError = err as ZodError;
                const errorCode = Number.parseInt(validationError.issues[0].message);
                throw new CulqiError(errorCode);
            }
        }
    };

    return {
        before,
    };
};

export default validateAndParseBodyMiddleware;
